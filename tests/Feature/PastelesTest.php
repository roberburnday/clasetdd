<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Pasteles;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class PastelesTest extends TestCase
{

    private $structureSaved = [
        'success',
        'pastel' => [
            'id',
            'name',
            'description',
            'user_id',
            'created_at',
            'updated_at'
        ]
    ];


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_doLoginPasteles(){
        //conseguir un usuario con pasteles para la prueba
        $users = User::select('users.*')
        ->leftJoin('pasteles','pasteles.user_id','=','users.id');
        $user = $users->whereNotNull('pasteles.id')->get()->random();
        //hacer el login del usuario con pasteles
        $token = "Bearer ".JWTAuth::fromUser($user);
        //comprobamos que el login funciona
        $response_auth_allow = $this->post('/api/user', [
            //datos del body
        ],[
            //datos del header
            'Authorization' => $token
        ]);
        $response_auth_allow->assertStatus(200);
        //retornamos el token
        return ['token'=>$token,'user'=>$user];
    }

    /**
     * @depends test_doLoginPasteles
     */
    public function test_list_show_pasteles($data){
        //mostrar listado pasteles
        $response_list = $this->get('/api/pasteles/list',[
            //datos del header
            'Authorization' => $data['token']
        ]);
        $response_list->assertStatus(200);
        $response_list->assertJsonStructure([
            [
                'id',
                'name',
                'description',
                'user_id',
                'created_at',
                'updated_at'
            ]
        ]);
        //show pastel me pertenece
        $pastelFromUser = Pasteles::where('user_id',$data['user']->id)->get()->random();
        $data['pastelOwned'] = $pastelFromUser;
        $response_show_allowed = $this->get('api/pasteles/show/'.$pastelFromUser->id,[
            //datos del header
            'Authorization' => $data['token']
        ]);
        $response_show_allowed->assertStatus(200);
        $response_show_allowed->assertJsonStructure([
            'id',
            'name',
            'description',
            'user_id',
            'created_at',
            'updated_at'
        ]);
        //show pastel no me pertenece
        $pastelNoFromUser = Pasteles::where('user_id','!=',$data['user']->id)->get()->random();
        $data['pastelNotOwned'] = $pastelNoFromUser;
        $response_show_notAllowed = $this->get('api/pasteles/show/'.$pastelNoFromUser->id,[
            //datos del header
            'Authorization' => $data['token']
        ]);
        $response_show_notAllowed->assertStatus(401);
        //retornamos los datos con los nuevos datos seleccionados, para evitar repetir consultas
        return $data;
    }

    /**
     * @depends test_list_show_pasteles
     */
    public function test_post_put_pastel($data){
        //test pastel con todos los datos
        $fakePastel = Pasteles::factory()->make([
            'user_id' => $data['user']->id
        ]);
        $response_create_ok = $this->post('/api/pasteles', [
            //datos del body
            'name' => $fakePastel->name,
            'description' => $fakePastel->description
        ],[
            //datos del header
            'Authorization' => $data['token']
        ]);
        $response_create_ok->assertStatus(200);
        $response_create_ok->assertJsonStructure(
            $this->structureSaved
        )->assertJson([
            'success' => true,
        ]);
        //test sin todos los datos

        //test edit pastel me pertenece
        $response_edit_ok = $this->put('/api/pasteles/'.$data['pastelOwned']->id, [
            //datos del body
            'name' => $fakePastel->name,
            'description' => $fakePastel->description
        ],[
            //datos del header
            'Authorization' => $data['token']
        ]);
        $response_edit_ok->assertStatus(200);
        $response_edit_ok->assertJsonStructure(
            $this->structureSaved
        )->assertJson([
            'success' => true,
        ]);
        //test edit pastel no me pertenece
        $response_edit_ok = $this->put('/api/pasteles/'.$data['pastelNotOwned']->id, [
            //datos del body
            'name' => $fakePastel->name,
            'description' => $fakePastel->description
        ],[
            //datos del header
            'Authorization' => $data['token']
        ]);
        $response_edit_ok->assertStatus(401);
    }

    /**
     * @depends test_list_show_pasteles
     */
    public function test_delete_pasteles($data){
        //dd($data['pastelOwned']->id);
        //test delete pastel me pertenece
        $response_delete_ok = $this->delete('/api/pasteles/'.$data['pastelOwned']->id,[
            //body de la peticion
        ],[
            //datos del header
            'Authorization' => $data['token']
        ]);
        $response_delete_ok->assertStatus(200);
        $response_delete_ok->assertJsonStructure(
            $this->structureSaved
        )->assertJson([
            'success' => true,
        ]);
        //test delete pastel no me pertenece
        $response_delete_notAllowed = $this->delete('/api/pasteles/'.$data['pastelNotOwned']->id,[
            //body de la peticion
        ],[
            //datos del header
            'Authorization' => $data['token']
        ]);
        $response_delete_notAllowed->assertStatus(401);
    }
}
