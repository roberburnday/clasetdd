<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;

//tests para comprobar las rutas

class UsersTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_login(){
        //conseguir el usuario
        $user = User::all()->random();

        //test auth no logged
        $response_auth_notAllow = $this->post('/api/user', []);
        $response_auth_notAllow->assertStatus(403);
        //probar un usuario no exisistente
        $response_login_fail = $this->post('/api/login', [
            'email' => 'noEmailExist@gmail.com',
            'password' => 'xxxxxxxxx'
        ]);
        $response_login_fail->assertStatus(400);
        //hacer login con un usuario real
        $token = "Bearer ".JWTAuth::fromUser($user);
        //test auth logged
        $response_auth_allow = $this->post('/api/user', [
            //datos del body
        ],[
            //datos del header
            'Authorization' => $token
        ]);
        $response_auth_allow->assertStatus(200);
        $response_auth_allow->assertJsonStructure([
            'user' => [
                'id',
                'name',
                'email',
                'email_verified_at',
                'created_at',
                'updated_at'
            ]
        ]);
    }
}
