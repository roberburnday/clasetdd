<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

use App\Models\Pasteles;
use Exception;

class PastelesController extends Controller
{
    //

    public function listPasteles(Request $request)
    {
        //filtros para el where en $request
        $pasteles = new Pasteles();
        $name = $request->input('name',null);
        $pasteles = $pasteles->where(function($query) use ($name){
            //esta function servirá para encapsular los filtros y evitar
            //que un "or" este al nivel del where de "user_id"
            if($name){
                $query->where('name','like','%'.$name.'%');
            }
        });
        //filtro para que no muestre pasteles de otros usuarios
        $pasteles = $pasteles->where('user_id',Auth::id());
        //retornar el resultado
        return Response::json($pasteles->get());
    }

    public function showPastel(Pasteles $id_pastel)
    {
        //TODO: proxima clase extraer en una function del service
        if($id_pastel->user_id != Auth::id()){
            return Response::json('not access',401);
        }
        //aquí no hace falta find porque lo esta ejecutando en el parametro
        return Response::json($id_pastel);
    }

    public function createPastel(Request $request){
        //TODO: próxima clase, sacar a service, explicar capas para extraer funcionalidad 
        //del controller. (No logica de negocio en controller)
        try{
            //se valida el request recibido
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255|unique:pasteles',
                'description' => 'required|string|max:255',
            ]);

            if($validator->fails()){
                return Response::json($validator->errors()->toArray(), 400);
            }
            //seteamos los valores y guardamos
            $pastel = new Pasteles();
            $pastel->fill($request->all());
            $pastel->user_id = Auth::id();
            $pastel->save();
            //retornamos
            return Response::json([
                'success' => true,
                'pastel' => $pastel
            ],200);
        }catch(Exception $e){
            return Response::json($e->getMessage(),500);
        }
    }

    public function editPastel(Pasteles $id_pastel, Request $request){
        //comprobamos que el pastel exista para el usuario
        //TODO: proxima clase extraer en una function del service
        // esta acción se realizará mucho
        if($id_pastel->user_id != Auth::id()){
            return Response::json('not access',401);
        }
        //empezamos la edición
        try{
            $id_pastel->fill($request->all());
            $id_pastel->save();
            return Response::json([
                'success' => true,
                'pastel' => $id_pastel
            ],200);
        }catch(Exception $e){
            //TODO: explicar helpers --> un poco deprecated --> evitaría repetición de código
            //todos los "return response" deberían estar estandarizados en una única función
            return Response::json($e->getMessage(),500);
        }
    }

    public function deletePastel(Pasteles $id_pastel){
        //TODO: proxima clase extraer en una function del service
        if($id_pastel->user_id != Auth::id()){
            return Response::json('not access',401);
        }
        //estas acciones estarían contenidas en el service de pasteles
        try{
            $id_pastel->delete();
            return Response::json([
                'success' => true,
                'pastel' => $id_pastel
            ],200);
        }catch(Exception $e){
            return Response::json($e->getMessage(),500);
        }
    }
}
