<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\PastelesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('register', [UserController::class,'register']);
Route::post('login', [UserController::class,'authenticate'])->middleware(["throttle:20,1"]);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('user',[UserController::class, 'getAuthenticatedUser']);
});

Route::group(['prefix' => 'pasteles', 'middleware' => 'jwt.verify'], function () {
    Route::get('list',[PastelesController::class,'listPasteles']);
    Route::get('show/{id_pastel}',[PastelesController::class,'showPastel']);
    Route::post('/',[PastelesController::class,'createPastel']);
    Route::put('/{id_pastel}',[PastelesController::class,'editPastel']);
    Route::delete('/{id_pastel}',[PastelesController::class,'deletePastel']);
});
