<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\User;

class PastelesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->regexify('[A-Za-z]{20}'),
            'description' => $this->faker->realText(180), //->regexify('[A-Za-z]{120}'),
            'user_id' => User::all()->random()->id //queremos setear un usuario existente
        ];
    }
}
