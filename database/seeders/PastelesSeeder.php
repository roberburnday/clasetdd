<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Pasteles;

class PastelesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Pasteles::factory()
            ->count(150)
            ->create();
    }
}
